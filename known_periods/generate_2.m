/////////////////////////////////////////////////////////////////////////
// generate_2.m
/////////////////////////////////////////////////////////////////////////
// Author: Tom Coates
/////////////////////////////////////////////////////////////////////////
// Magma code to generate the key-value file Fano_2.txt
// This records the period sequences and Picard--Fuchs operators for
// del Pezzo surfaces.

// Build the output filename
fs_root:=GetEnv("FSROOT");
error if #fs_root eq 0, "The environment variable FSROOT must be set.";
F := NewKeyValueFile(fs_root cat "/db/known_periods/Fano_2.txt");

// Build the Weyl algebra
W<D,t> := WeylAlgebra();

// The del Pezzo surfaces all have toric complete intersection models:
// see section G of Coates--Corti--Galkin--Kasprzyk, Quantum periods
// for 3-dimensional Fano manifolds. Geom. Topol. 20 (2016), no. 1,
// 103--256.
// We record these as tuples: <name, weight data, bundle data>
mirrors := [
	    <"P2", [[1,1,1]], []>,
	    <"P1 x P1", [[1,1,0,0],[0,0,1,1]], []>,
	    <"dP8", [[1,1,-1,0],[0,0,1,1]], []>,
	    <"dP7", [[1,0,1,-1,0],[0,1,1,0,-1],[0,0,-1,1,1]], []>,
	    <"dP6", [[1,0,0,0,1,-1],[0,1,0,0,1,0],[0,0,1,0,0,1],[0,0,0,1,-1,1]], []>,
	    <"dP5", [[1,1,0,0,0],[0,0,1,1,1]], [[1,2]]>,
	    <"dP4", [[1,1,1,1,1]], [[2],[2]]>,
	    <"dP3", [[1,1,1,1]], [[3]]>,
	    <"dP2", [[1,1,1,2]], [[4]]>,
	    <"dP1", [[1,1,2,3]], [[6]]>
	    ];


// Proof data for the del Pezzo surfaces
proofs := AssociativeArray();
proofs["P2"] := "Mirror symmetry for projective space: Givental, Equivariant Gromov-Witten invariants.  Internat. Math. Res. Notices  1996,  no. 13, 613--663";
proofs["dP4"] := "dP4 is a (2,2) complete intersection in P4.  Now use mirror symmetry for projective complete intersections: Givental, Equivariant Gromov-Witten invariants.  Internat. Math. Res. Notices  1996,  no. 13, 613--663";
proofs["dP3"] := "dP3 is a cubic surface in P3.  Now use mirror symmetry for projective complete intersections: Givental, Equivariant Gromov-Witten invariants.  Internat. Math. Res. Notices  1996,  no. 13, 613--663";
proofs["dP2"] := "dP2 is a quartic surface in P(1,1,1,2).  Now use mirror symmetry for weighted projective complete intersections: Coates--Corti--Lee--Tseng, The quantum orbifold cohomology of weighted projective spaces. Acta Math. 202 (2009), no. 2, 139--193";
proofs["dP1"] := "dP1 is a sextic surface in P(1,1,2,3).  Now use mirror symmetry for weighted projective complete intersections: Coates--Corti--Lee--Tseng, The quantum orbifold cohomology of weighted projective spaces. Acta Math. 202 (2009), no. 2, 139--193";

for T in mirrors do
    name := T[1];
    W := T[2];
    B := T[3];
    ps := PeriodSequenceForCompleteIntersection(W, B, 50);
    PF := PicardFuchsOperator(ps);
    f := LaurentForCompleteIntersection(W,B);
    _<x,y> := Parent(f);
    data := AssociativeArray();
    data["period"] := ps[1..16];
    data["PF"] := PF;
    data["names"] := ["\"" cat name cat "\""];
    ok, details := IsDefined(proofs, name);
    if ok then
	data["proven"] := true;
	data["proof"] := "\"" cat details cat "\"";
    else
	data["proven"] := false;
	data["proof"] := "\"\"";
    end if;
    data["f"] := PrintLaurent(f);
    KeyValueWrite(F,data);
    KeyValueFlush(F);
end for;
