/////////////////////////////////////////////////////////////////////////
// generate_4_step_1.m
/////////////////////////////////////////////////////////////////////////
// Author: Tom Coates
/////////////////////////////////////////////////////////////////////////
// Magma code.  Together with generate_4_step_2.m, this generates the
// key-value file Fano_4.txt which records the period sequences and
// Picard--Fuchs operators for known four-dimensional Fano manifolds

// This script generates the key-value file "4d_Lairez_input.txt"
// That file should then be used as input to hpcjobs/Lairez, and the
// output from that aggregated as the file "4d_Lairez_output.txt"
// generate_4_step_2.m needs "4d_Lairez_output.txt" as input data.
//
// A one-liner to split the file "4d_Lairez_input.txt" on blank lines,
// resulting in 1.txt, 2.txt, ... is:
//
// awk -v RS= '{print > (NR ".txt"); close(NR ".txt")}' 4d_Lairez_input.txt

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// Return the directory that contains the period sequence key-value
// databases.  It raises a runtime error if the environment variable
// FSROOT is not set
function get_db_dir()
    fs_root:=GetEnv("FSROOT");
    error if #fs_root eq 0, "The environment variable FSROOT must be set.";
    return fs_root cat "/db/known_periods";
end function;

// Read the key-value database Fano_N
function read_Fano(N)
    // W is the Weyl algebra that contains the Picard--Fuchs operators
    W<D,t> := WeylAlgebra();
    // R is the ring that holds the Laurent polynomial mirrors
    case N:
    when 1:
	R<x> := RationalFunctionField(Rationals(),1);
    when 2:
	R<x,y> := RationalFunctionField(Rationals(),2);
    when 3:
	R<x,y,z> := RationalFunctionField(Rationals(),3);
    end case;
    // Build the conversion functions
    keys := {"period", "names", "PF", "proven", "proof", "f"};
    conversion_funcs := AssociativeArray();
    for k in keys do
	conversion_funcs[k] := func<xx|eval(xx)>;
    end for;
    // Read the database and return
    input_file := Sprintf("%o/Fano_%o.txt", get_db_dir(), N);
    return [xx : xx in KeyValueFileProcess(input_file, keys : skip_unknown:=false, require_all_keys:=true, conversion_funcs:=conversion_funcs)];
end function;

// Return the product of Fano manifolds X and Y, each of which should be an associative array with keys "period", "names", "PF", "proven", "proof", "f".  The dimensions of X and Y should sum to 4.
function build_product(X, Y)
    // Build the mirror Laurent polynomial
    g := X["f"];
    h := Y["f"];
    assert Rank(Parent(g)) + Rank(Parent(h)) eq 4;
    R<x,y,z,w> := RationalFunctionField(Rationals(),4);
    phi := hom<Parent(g)->R|[R.i : i in [1..Rank(Parent(g))]]>;
    psi := hom<Parent(h)->R|[R.i : i in [1+Rank(Parent(g))..4]]>;
    f := phi(g) + psi(h);
    names := [ a cat " x " cat b : a in X["names"], b in Y["names"] ];
    // build the result and return
    data := AssociativeArray();
    data["period"] := PeriodSequence(f,15);
    data["f"] := f;
    data["names"] := ["\"" cat n cat "\"" : n in names];
    data["proven"] := false;
    data["proof"] := "\"\"";
    printf "Built product with names %o\n", names;
    return data;
end function;

// Return the four-dimensional Fano manifold Obro(N).
function Obro(N)
    P:=PolytopeSmoothFanoDim4(N);
    f:=PolytopeToLaurent(P,BinomialEdgeCoefficients(P));
    _<x,y,z,w> := Parent(f);
    ps:=PeriodSequence(f,15);
    data := AssociativeArray();
    data["f"] := f;
    data["period"] := ps;
    data["names"] := [ Sprintf("\"Obro(4,%o)\"", N) ];
    data["proven"] := false;
    data["proof"] := "\"\"";
    printf "Built Obro (%o / 124)\n", N;
    return data;
end function;

// data_for_ci returns the toric complete intersection with weights W, bundle data B, and the specified names.  
function data_for_ci(W,B,names : allow_weak_fano:=false)
    f := LaurentForCompleteIntersection(W,B : define_fan:=true, allow_weak_fano:=allow_weak_fano);
    _<x,y,z,w> := Parent(f);
    data := AssociativeArray();
    data["period"] := PeriodSequence(f,15);
    data["f"] := f;
    data["names"] := names;
    data["proven"] := false;
    data["proof"] := "\"\"";
    printf "Built toric c.i. with names %o\n", names;
    return data;
end function;

// Return the index in seq of the Fano manifold with the given name, or 0 if there is no such Fano manifold.
function index_by_name(seq, name)
    candidates := [ i : i in [1..#seq] | name in seq[i]["names"] ];
    error if #candidates gt 1, "The sequence contains more than one Fano manifold with the given name";
    if #candidates eq 0 then
	return 0;
    else
	return candidates[1];
    end if;
end function;

// Return the Fano manifold in seq with the given name.
function lookup_by_name(seq, name)
    idx := index_by_name(seq, name);
    error if idx eq 0, "The sequence contains no Fano manifold with the given name";
    return seq[idx];
end function;

// Return the index in seq of the Fano manifold with the given sequence of names, or 0 if there is no such Fano manifold.
function index_by_exact_names(seq, names)
    candidates := [ i : i in [1..#seq] | seq[i]["names"] eq names ];
    error if #candidates gt 1, "The sequence contains more than one Fano manifold with the given names";
    if #candidates eq 0 then
	return 0;
    else
	return candidates[1];
    end if;
end function;

// Regularise an unregularised period sequence.
function regularise(ps)
    return [Integers() | Factorial(i - 1) * ps[i] : i in [1..#ps]];
end function;

// Unregularise a regularised period sequence.
function unregularise(ps)
    return [Rationals() | ps[i] / Factorial(i - 1) : i in [1..#ps]];
end function;

// Return the product of the period sequences
function product_ps(ps1, ps2)
    len := Max(#ps1, #ps2);
    R := PolynomialRing(Rationals());
    return Coefficients((R!ps1)*(R!ps2))[1..len];
end function;

//////////////////////////////////////////////////////////////////////
// Building the key-value file
//////////////////////////////////////////////////////////////////////

// Read in the lower-dimensional Fano manifolds
Fano1 := read_Fano(1);
Fano2 := read_Fano(2);
Fano3 := read_Fano(3);

// start with P1 x M where M is a 3-dimensional Fano manifold
results := [build_product(X,Y) : X in Fano1, Y in Fano3];

// add the products of del Pezzo surfaces
for i in [1..#Fano2] do
    for j in [i..#Fano2] do
	Append(~results, build_product(Fano2[i], Fano2[j]));
    end for;
end for;

// add the smooth toric Fano 4-folds
for N in [1..124] do
    Append(~results, Obro(N));
end for;

// P^4
Append(~results, data_for_ci([[1,1,1,1,1]],[],["\"P4\""]));

// Q^4
Append(~results, data_for_ci([[1,1,1,1,1,1]],[[2]],["\"Q4\""]));

////////////////////////////////////////////////////////////////////////////////
// index 3: the complete list, according to Fujita and Iskovskikh (see
// Prokhorov--Iskovskikh chapter 3) is:
// FI(1): a sextic in P(3,2,1,1,1,1)
// FI(2): a quartic in P(2,1,1,1,1,1)
// FI(3): a cubic in P^5
// FI(4): a (2,2) complete intersection in P^6
// FI(5): a (1,1) complete intersection in Gr(2,5)
// FI(6): P^2 x P^2
////////////////////////////////////////////////////////////////////////////////
Append(~results, data_for_ci([[3,2,1,1,1,1]],[[6]],["\"FI(1)\""]));
Append(~results, data_for_ci([[2,1,1,1,1,1]],[[4]],["\"FI(2)\""]));
Append(~results, data_for_ci([[1,1,1,1,1,1]],[[3]],["\"FI(3)\""]));
Append(~results, data_for_ci([[1,1,1,1,1,1,1]],[[2],[2]],["\"FI(4)\""]));
// For FI(5) we use Prince's Appendix in [https://arxiv.org/pdf/1409.3729v2.pdf]
Append(~results, data_for_ci([[1,0,0,0,0,1,1,1,1],[1,1,0,0,1,0,0,1,1],[1,1,1,1,0,0,0,0,1]],[[1,1,1],[1,1,1]],["\"FI(5)\""]));
Append(~results, data_for_ci([[1,1,1,0,0,0],[0,0,0,1,1,1]],[],["\"FI(6)\""]));

////////////////////////////////////////////////////////////////////////////////
// index 2, Picard rank 1
// There are 9 cases: the "unsections" of V^3_2, V^3_4,...,V^3_{18}
////////////////////////////////////////////////////////////////////////////////

// V(4,2) is a sextic hypersurface in P(1,1,1,1,1,3)
Append(~results, data_for_ci([[1,1,1,1,1,3]],[[6]],["\"V(4,2)\""]));

// V(4,4) is a quartic hypersurface in P^5
Append(~results, data_for_ci([[1,1,1,1,1,1]],[[4]],["\"V(4,4)\""]));

// V(4,6) is a (2,3) complete intersection in P^6
Append(~results, data_for_ci([[1,1,1,1,1,1,1]],[[2],[3]],["\"V(4,6)\""]));

// V(4,8) is a (2,2,2) complete intersection in P^7
Append(~results, data_for_ci([[1,1,1,1,1,1,1,1]],[[2],[2],[2]],["\"V(4,8)\""]));

// V^(4,10) is a (1,2) complete intersection in Gr(2,5).  We use Prince's Appendix in [https://arxiv.org/pdf/1409.3729v2.pdf] again.
Append(~results, data_for_ci([[1,0,0,0,0,1,1,1,1],[1,1,0,0,1,0,0,1,1],[1,1,1,1,0,0,0,0,1]],[[1,1,1],[2,2,2]],["\"V(4,10)\""]));

// I don't know of a model for V(4,12) from which I can compute a Laurent polynomial, so we compute this as an unsection in step 2.

// V^(4,14) is a (1,1,1,1) complete intersection in Gr(2,6).  We use Prince's Appendix in [https://arxiv.org/pdf/1409.3729v2.pdf] again.
Append(~results, data_for_ci([[1,0,0,0,0,0,0,1,1,1,1,1],[1,1,0,0,0,0,1,0,0,1,1,1],[1,1,1,0,0,1,0,0,0,0,1,1],[1,1,1,1,1,0,0,0,0,0,0,1]],[[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]],["\"V(4,14)\""]));

// We compute V(4,16) and V(4,18) as unsections in step 3.  Again, I don't know of models from which I can compute a Laurent polynomial.

////////////////////////////////////////////////////////////////////////////////
// index 2, Picard rank > 1
// There are 18 families, as described in Table 12.7 of Iskovskikh--Prokhorov
// We denote them by MW(k), k in [1..18]; they were classified by
// Mukai--Wisniewski
////////////////////////////////////////////////////////////////////////////////

// MW(1) is P^1 x B^3_1, and B^3_1 is a sextic hypersurface in P^4(1,1,1,2,3)
Append(~results, data_for_ci([[1,1,1,2,3,0,0],[0,0,0,0,0,1,1]],[[6,0]],["\"MW(1)\""]));

// MW(2) is P^1 x B^3_2, and B^3_2 is a quartic hypersurface in P^4(1,1,1,1,2)
Append(~results, data_for_ci([[1,1,1,1,2,0,0],[0,0,0,0,0,1,1]],[[4,0]],["\"MW(2)\""]));

// MW(3) is P^1 x B^3_3, and B^3_3 is a cubic hypersurface in P^4
Append(~results, data_for_ci([[1,1,1,1,1,0,0],[0,0,0,0,0,1,1]],[[3,0]],["\"MW(3)\""]));

// MW(4) is a toric complete intersection, but the calculation is subtle: see Coates--Galkin--Kasprzyk--Strangeway section 6.2.4 for details
Append(~results, data_for_ci([[1,1,1,0,0,0,1],[0,0,0,1,1,1,1]],[[2,2]],["\"MW(4)\""]));

// MW(5) is a divisor on P^2 x P^3 of bidegree (1,2)
Append(~results, data_for_ci([[1,1,1,0,0,0,0],[0,0,0,1,1,1,1]],[[1,2]],["\"MW(5)\""]));

// MW(6) is P^1 x B^3_4, and B^3_4 is a (2,2) complete intersection in P^5
Append(~results, data_for_ci([[1,1,1,1,1,1,0,0],[0,0,0,0,0,0,1,1]],[[2,0],[2,0]],["\"MW(6)\""]));

// MW(7) is a complete intersection of two divisors in P^3 x P^3, each of bidegree (1,1)
Append(~results, data_for_ci([[1,1,1,1,0,0,0,0],[0,0,0,0,1,1,1,1]],[[1,1],[1,1]],["\"MW(7)\""]));

// MW(8) is a divisor on P^2 x Q^3 of bidegree (1,1)
Append(~results, data_for_ci([[1,1,1,0,0,0,0,0],[0,0,0,1,1,1,1,1]],[[1,1],[0,2]],["\"MW(8)\""]));

// MW(9) is P1 x B^3_5.  We use Prince's Appendix in [https://arxiv.org/pdf/1409.3729v2.pdf] again.
Append(~results, data_for_ci([[1,0,0,0,0,1,1,1,1,0,0],[1,1,0,0,1,0,0,1,1,0,0],[1,1,1,1,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,0,0,1,1]],[[1,1,1,0],[1,1,1,0],[1,1,1,0]],["\"MW(9)\""]));

// MW(10) is a toric complete intersection
Append(~results, data_for_ci([[1,1,1,-1,0,0,0],[0,0,0,1,1,1,1]],[[0,2]],["\"MW(10)\""]));

// I don't know a model for MW(11) from which we can compute a Laurent polynomial

// MW(12) is a toric complete intersection
Append(~results, data_for_ci([[1,1,1,1,-1,0,0],[0,0,0,0,1,1,1]],[[1,1]],["\"MW(12)\""]));

// MW(13) is a toric complete intersection
Append(~results, data_for_ci([[1,1,1,1,1,0,-1],[0,0,0,0,0,1,1]],[[2,0]],["\"MW(13)\""]));

// MW(14) is P^1 x P^3
Append(~results, data_for_ci([[1,1,1,1,0,0],[0,0,0,0,1,1]],[],["\"MW(14)\""]));

// MW(15) is a toric variety
Append(~results, data_for_ci([[1,1,1,1,0,-2],[0,0,0,0,1,1]],[],["\"MW(15)\""]));

// MW(16) is P^1 x W^3, where W^3 is a (1,1) hypersurface in P^2 x P^2
Append(~results, data_for_ci([[1,1,1,0,0,0,0,0],[0,0,0,1,1,1,0,0],[0,0,0,0,0,0,1,1]],[[1,1,0]],["\"MW(16)\""]));

// MW(17) is P^1 x B^3_7, where B^3_7 is the blow-up of P^3 in a point
Append(~results, data_for_ci([[1,1,1,0,-1,0,0],[0,0,0,1,1,0,0],[0,0,0,0,0,1,1]],[],["\"MW(17)\""]));

// MW^4_18 is P^1 x P^1 x P^1 x P^1
Append(~results, data_for_ci([[1,1,0,0,0,0,0,0],[0,0,1,1,0,0,0,0],[0,0,0,0,1,1,0,0],[0,0,0,0,0,0,1,1]],[],["\"MW(18)\""]));

//////////////////////////////////////////////////////////////////////
// add the Picard--Fuchs operators for some special cases in which
// Lairez's algorithm is expensive
//////////////////////////////////////////////////////////////////////

printf "Computing Picard--Fuchs operators for special cases\n";

// V(4,2) is a sextic in P(1,1,1,1,1,3)
W := [[1,1,1,1,1,3]];
B := [[6]];
ps := PeriodSequenceForCompleteIntersection(W,B,200);
PF := PicardFuchsOperator(ps);
idx := index_by_name(results, "\"V(4,2)\"");
this_data := results[idx];
this_data["PF"] := PF;
results[idx] := this_data;
printf "Computed Picard--Fuchs operator for special case with name %o\n", "V(4,2)";

// the remaining special cases are products.  We grab the factors.
P1 :=  lookup_by_name(Fano1, "P1");
dP1 := lookup_by_name(Fano2, "dP1");
dP2 := lookup_by_name(Fano2, "dP2");
dP3 := lookup_by_name(Fano2, "dP3");
dP4 := lookup_by_name(Fano2, "dP4");
dP5 := lookup_by_name(Fano2, "dP5");
dP6 := lookup_by_name(Fano2, "dP6");
dP7 := lookup_by_name(Fano2, "dP7");
dP8 := lookup_by_name(Fano2, "dP8");
V32 := lookup_by_name(Fano3, "V(3,2)");
MM21 := lookup_by_name(Fano3, "MM(2,1)");
MM212 := lookup_by_name(Fano3, "MM(2,12)");
P1xdP1 := lookup_by_name(Fano3, "P1 x dP1");
special_cases := [[* ["\"P1 x V(3,2)\""], P1, V32 *],
		  [* ["\"P1 x MM(2,1)\""], P1, MM21 *],
		  [* ["\"P1 x MM(2,12)\""], P1, MM212 *],
		  [* ["\"P1 x P1 x dP1\""], P1, P1xdP1 *],
		  [* ["\"P1 x MM(10,1)\"","\"P1 x P1 x dP1\""], P1, P1xdP1 *],
		  [* ["\"dP8 x dP1\""], dP8, dP1 *],
		  [* ["\"dP7 x dP1\""], dP7, dP1 *],
 		  [* ["\"dP6 x dP1\""], dP6, dP1 *],
		  [* ["\"dP5 x dP1\""], dP5, dP1 *],
		  [* ["\"dP4 x dP1\""], dP4, dP1 *],
		  [* ["\"dP3 x dP1\""], dP3, dP1 *],
		  [* ["\"dP2 x dP1\""], dP2, dP1 *],
		  [* ["\"dP1 x dP1\""], dP1, dP1 *],
		  [* ["\"dP2 x dP2\""], dP2, dP2 *]
		  ];
for x in special_cases do
    names, X ,Y := Explode(x);
    unreg1 := unregularise(PeriodSequenceForPicardFuchsOperator(X["PF"], 500, X["period"]));
    unreg2 := unregularise(PeriodSequenceForPicardFuchsOperator(Y["PF"], 500, Y["period"]));
    ps := regularise(product_ps(unreg1, unreg2));
    PF := PicardFuchsOperator(ps);
    idx := index_by_exact_names(results, names);
    this_data := results[idx];
    this_data["PF"] := PF;
    results[idx] := this_data;
    printf "Computed Picard--Fuchs operator for special case with names %o\n", names;
end for;

// write out the key-value file
F := NewKeyValueFile(Sprintf("%o/4d_Lairez_input.txt", get_db_dir()));
for data in results do
    KeyValueWrite(F, data);
    KeyValueFlush(F);
end for;
