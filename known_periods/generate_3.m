/////////////////////////////////////////////////////////////////////////
// generate_3.m
/////////////////////////////////////////////////////////////////////////
// Author: Tom Coates
/////////////////////////////////////////////////////////////////////////
// Magma code to generate the key-value file Fano_3.txt
// This records the period sequences and Picard--Fuchs operators for
// three-dimensional Fano manifolds

// Build the output filename
fs_root:=GetEnv("FSROOT");
error if #fs_root eq 0, "The environment variable FSROOT must be set.";
F := NewKeyValueFile(fs_root cat "/db/known_periods/Fano_3.txt");

// Record the names associated to each mutation-equivalence class of
// Minkowski polynomials
Minkowski_names:=[
		  <1, ["P3"]>,
		  <2, ["MM(2,33)"]>,
		  <3, ["Q3"]>,
		  <4, ["MM(2,30)"]>,
		  <5, ["MM(2,28)"]>,
		  <6, ["MM(2,36)"]>,
		  <7, ["MM(2,35)"]>,
		  <8, ["MM(3,29)"]>,
		  <10, ["MM(2,34)", "P1 x P2"]>,
		  <11, ["MM(3,30)"]>,
		  <12, ["MM(3,26)"]>,
		  <13, ["MM(3,22)"]>,
		  <14, ["MM(3,31)"]>,
		  <15, ["MM(2,31)"]>,
		  <16, ["MM(3,25)"]>,
		  <17, ["MM(3,23)"]>,
		  <18, ["MM(3,19)"]>,
		  <19, ["MM(2,27)"]>,
		  <21, ["MM(3,14)"]>,
		  <22, ["MM(3,9)"]>,
		  <24, ["MM(2,32)"]>,
		  <28, ["MM(3,28)", "P1 x dP8"]>,
		  <29, ["MM(4,13)"]>,
		  <31, ["MM(3,24)"]>,
		  <34, ["MM(4,12)"]>,
		  <35, ["MM(2,29)"]>,
		  <37, ["MM(4,10)"]>,
		  <38, ["MM(3,20)"]>,
		  <39, ["MM(3,17)"]>,
		  <41, ["MM(3,18)"]>,
		  <42, ["MM(3,16)"]>,
		  <43, ["MM(2,25)"]>,
		  <44, ["MM(2,24)"]>,
		  <45, ["MM(3,27)", "P1 x P1 x P1"]>,
		  <46, ["B(3,5)"]>,
		  <48, ["MM(4,11)", "P1 x dP7"]>,
		  <49, ["MM(3,21)"]>,
		  <54, ["MM(4,9)"]>,
		  <57, ["MM(4,8)"]>,
		  <58, ["MM(2,26)"]>,
		  <64, ["MM(5,2)"]>,
		  <65, ["MM(4,7)"]>,
		  <67, ["MM(3,15)"]>,
		  <68, ["MM(4,5)"]>,
		  <69, ["MM(2,22)"]>,
		  <70, ["MM(3,13)"]>,
		  <72, ["MM(3,11)"]>,
		  <74, ["MM(2,18)"]>,
		  <75, ["B(3,4)"]>,
		  <76, ["MM(5,3)", "P1 x dP6"]>,
		  <78, ["MM(2,23)"]>,
		  <81, ["MM(4,6)"]>,
		  <83, ["MM(4,4)"]>,
		  <84, ["MM(2,21)"]>,
		  <85, ["MM(3,12)"]>,
		  <86, ["MM(2,19)"]>,
		  <87, ["MM(2,20)"]>,
		  <88, ["MM(4,3)"]>,
		  <99, ["MM(3,10)"]>,
		  <100, ["MM(5,1)"]>,
		  <101, ["MM(2,17)"]>,
		  <103, ["MM(3,7)"]>,
		  <104, ["MM(2,16)"]>,
		  <106, ["B(3,3)"]>,
		  <107, ["MM(6,1)", "P1 x dP5"]>,
		  <109, ["MM(2,15)"]>,
		  <110, ["MM(4,2)"]>,
		  <111, ["MM(4,1)"]>,
		  <112, ["MM(3,8)"]>,
		  <113, ["V(3,22)"]>,
		  <117, ["MM(3,6)"]>,
		  <118, ["MM(2,12)"]>,
		  <119, ["MM(2,13)"]>,
		  <120, ["MM(2,11)"]>,
		  <122, ["MM(2,14)"]>,
		  <124, ["V(3,18)"]>,
		  <135, ["MM(3,3)"]>,
		  <136, ["MM(7,1)", "P1 x dP4"]>,
		  <138, ["MM(3,5)"]>,
		  <139, ["MM(2,9)"]>,
		  <140, ["B(3,2)"]>,
		  <142, ["MM(3,4)"]>,
		  <143, ["V(3,16)"]>,
		  <144, ["MM(2,8)"]>,
		  <145, ["MM(2,10)"]>,
		  <147, ["V(3,14)"]>,
		  <148, ["MM(2,7)"]>,
		  <149, ["MM(2,6)"]>,
		  <150, ["V(3,12)"]>,
		  <154, ["MM(3,1)"]>,
		  <155, ["MM(8,1)", "P1 x dP3"]>,
		  <157, ["MM(3,2)"]>,
		  <158, ["MM(2,5)"]>,
		  <160, ["V(3,10)"]>,
		  <161, ["MM(2,4)"]>,
		  <163, ["V(3,8)"]>,
		  <164, ["V(3,6)"]>,
		  <165, ["V(3,4)"]> ];


// Build the Weyl algebra
W<D,t> := WeylAlgebra();

// Read in the Minkowski data
in_file := fs_root cat "/db/known_periods/Minkowski.txt";
keys := {"period", "PF", "id"};
conversion_funcs := AssociativeArray();
for k in keys do
    conversion_funcs[k] := func<xx|eval(xx)>;
end for;
Minkowski_data := [ xx : xx in KeyValueFileProcess(in_file, keys : skip_unknown:=false, require_all_keys:=true, conversion_funcs := conversion_funcs) ];

// handle the Minkowski cases, that is, the 98 cases with very ample
// anticanonical bundle
for T in Minkowski_names do
    id := T[1];
    names := T[2];
    reps := [xx : xx in Minkowski_data | xx["id"] eq id];
    error if not #reps eq 1, "There should be exactly one Minkowski representative!";
    data := reps[1];
    f := Representative(LatticeMinkowskiBucketDim3(id));
    _<x,y,z> := Parent(f);
    this_data := AssociativeArray();
    this_data["period"] := data["period"];
    this_data["PF"] := data["PF"];
    this_data["f"] := f;
    this_data["names"] := ["\"" cat x cat "\"" : x in names];
    this_data["proven"] := false;
    this_data["proof"] := "\"\"";
    KeyValueWrite(F,this_data);
    KeyValueFlush(F);
end for;

// write_data_for_ci writes to the key-value file F the data for the toric complete intersection with weights W, bundle data B, and the specified names.  It flushes the key-value file before returning.
procedure write_data_for_ci(F,W,B,names)
    ps := PeriodSequenceForCompleteIntersection(W,B,100 : define_fan:=true);
    PF := PicardFuchsOperator(ps);
    f := LaurentForCompleteIntersection(W,B : define_fan:=true);
    _<x,y,z> := Parent(f);
    // build the key-value record
    data := AssociativeArray();
    data["period"] := ps[1..16];
    data["PF"] := PF;
    data["f"] := f;
    data["names"] := names;
    data["proven"] := false;
    data["proof"] := "\"\"";
    // sanity check
    assert IsZero(ApplyPicardFuchsOperator(PF,ps));
    // write the data to F
    KeyValueWrite(F,data);
    KeyValueFlush(F);
end procedure;


// B1 is a sextic hypersurface in P(1,1,1,2,3)
write_data_for_ci(F,[[1,1,1,2,3]],[[6]],["\"B(3,1)\""]);

// V2 is a sextic hypersurface in P(1,1,1,1,3)
write_data_for_ci(F,[[1,1,1,1,3]],[[6]],["\"V(3,2)\""]);

// Coates--Corti--Galkin--Kasprzyk section 18 treats MM(2,1)
write_data_for_ci(F,[[1,1,1,2,3,0,0],[0,0,0,0,0,1,1]],[[6,0],[1,1]],["\"MM(2,1)\""]);

// Coates--Corti--Galkin--Kasprzyk section 19 treats MM(2,2)
write_data_for_ci(F,[[1,1,0,0,0,1],[0,0,1,1,1,2]],[[2,4]],["\"MM(2,2)\""]);

// Coates--Corti--Galkin--Kasprzyk section 20 treats MM(2,3)
write_data_for_ci(F,[[1,1,1,1,2,0,0],[0,0,0,0,0,1,1]],[[4,0],[1,1]],["\"MM(2,3)\""]);

// Coates--Corti--Galkin--Kasprzyk section 104 treats MM(9,1)
write_data_for_ci(F,[[1,1,1,2,0,0],[0,0,0,0,1,1]],[[4,0]],["\"MM(9,1)\"", "\"P1 x dP2\""]);

// Coates--Corti--Galkin--Kasprzyk section 105 treats MM(10,1)
write_data_for_ci(F,[[1,1,2,3,0,0],[0,0,0,0,1,1]],[[6,0]],["\"MM(10,1)\"", "\"P1 x dP1\""]);
