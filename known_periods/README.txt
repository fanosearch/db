This directory contains information about known period sequences and Picard--Fuchs operators.

Minkowski.txt
=========

This file is in key-value format, with the following keys.  Each entry represents a mutation-equivalence class of 3-dimensional Minkowski polynomials.

id:         the id in the Graded Ring Database.  See [http://grdb.co.uk/forms/period3].
period:  the first few terms of the period sequence
PF:        the Picard--Fuchs operator, as an element of the Weyl algebra W = QQ<D,t>

Fano_N.txt
=======

This file is in key-value format, with the following keys.  Each entry represents a known N-dimensional Fano manifold X.

period:   the first few terms of the period sequence of X
PF:         the Picard--Fuchs operator of X, as an element of the Weyl algebra W = QQ<D,t>
names:   a sequence of strings, each of which is a name that identifies X.  Our naming conventions are discussed below.
proven:  a boolean indicating whether or not the period sequence and Picard--Fuchs operators have been proven to be correct.
proof:    a string giving a sketch of the proof (if any)
f:           a Laurent polynomial that is mirror to X.  This key may or may not be present.

If N eq 1, f is an element of QQ[x].  If n eq 2, f is an element of QQ[x,y].  If n eq 3, f is an element of QQ[x,y,z].  If n eq 4, f is an element of QQ[x,y,z,w].

Naming conventions
============

PN         N-dimensional projective space
X x Y      the product of X and Y
dPk        the blow-up of P2 at 9-k points
QN         the N-dimensional quadric
B(3,k)     the 3-dimensional Fano manifold with Picard rank 1, Fano index 2, and degree 8k
V(3,k)     the 3-dimensional Fano manifold with Picard rank 1, Fano index 1, and degree k
MM(r,k)  the k-th entry in the Mori--Mukai list of three-dimensional Fano manifolds of Picard
              rank r. We use the the ordering as in Coates--Corti--Galkin--Kasprzyk, Quantum periods
	      for 3-dimensional Fano manifolds. Geom. Topol. 20 (2016), no. 1, 103--256, which
	      agrees with the original papers of Mori--Mukai except when r=4.
FI(k)       the k-th 4-dimensional Fano manifold of index 3.  We use the ordering as in
              Coates--Galkin--Kasprzyk--Strangeway, "Quantum Periods for Certain Four-Dimensional
	      Fano Manifolds".
V(4,k)     the 4-dimensional Fano manifold with Picard rank 1, Fano index 2, and degree 16k
MW(k)    the k-th 4-dimensional Fano manifold of index 2 with Picard rank greater than 1.  We use
              the ordering as in Coates--Galkin--Kasprzyk--Strangeway.
Obro(4,k)  the k-th smooth four-dimensional toric Fano variety, with ordering as in Obro, "An
                algorithm for the classification of smooth Fano polytopes", arXiv:0704.0049.  This is the
		(23+k)-th Fano toric manifold in the Graded Ring Database [http://www.grdb.co.uk]
CKP(k)    the k-th four-dimensional Fano toric complete intersection, with ordering as in
              Coates--Kasprzyk--Prince, Four-dimensional Fano toric complete intersections.
	      Proc. Royal Society A. Vol. 471. No. 2175.
Str(k)     the k-th Strangeway 4-fold, as in Coates--Galkin--Kasprzyk--Strangeway, "Quantum
              Periods for Certain Four-Dimensional Fano Manifolds".



generate_2.m
=========

This is Magma code to generate the key-value file Fano_2.txt


generate_3.m
=========

This is Magma code to generate the key-value file Fano_3.txt


generate_4_step_1.m and generate_4_step_2.m
===============================

These contain Magma code to generate the key-value file Fano_4.txt
