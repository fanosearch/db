/////////////////////////////////////////////////////////////////////////
// generate_4_step_2.m
/////////////////////////////////////////////////////////////////////////
// Author: Tom Coates
/////////////////////////////////////////////////////////////////////////
// Magma code.  Together with generate_4_step_1.m, this generates the
// key-value file Fano_4.txt which records the period sequences and
// Picard--Fuchs operators for known four-dimensional Fano manifolds

// The script generate_4_step_1.m generates the key-value file
// "4d_Lairez_input.txt".  That file should then be used as input to
// hpcjobs/Lairez, and the output from that aggregated as the file
// "4d_Lairez_output.txt"

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// Return the Fanosearch root directory.  It raises a runtime error
// if the environment variable FSROOT is not set
function get_fs_root()
    fs_root:=GetEnv("FSROOT");
    error if #fs_root eq 0, "The environment variable FSROOT must be set.";
    return fs_root;
end function;

// Return the directory that contains the period sequence key-value
// databases.  It raises a runtime error if the environment variable
// FSROOT is not set
function get_db_dir()
    return get_fs_root() cat "/db/known_periods";
end function;

// Return the directory that contains the toric complete intersection
// data.  It raises a runtime error if the environment variable
// FSROOT is not set
function get_ci_dir()
    return get_fs_root() cat "/papers/toric_complete_intersections_dim_4/data";
end function;

// Merge the Fano manifold M into the sequence L of Fano manifolds.
procedure merge_in(~L, M)
    ps := M["period"][1..16];
    names := M["names"];
    // idxs is the indices in L of Fano manifolds with matching period sequence
    idxs := [ i : i in [1..#L] | L[i]["period"][1..16] eq ps ];
    if #idxs eq 0 then
	Append(~L,M);
	printf "Adding new Fano manifold %o\n", names;
	return;
    elif #idxs gt 1 then
	error "Non-unique period sequences in L";
    end if;
    // check the Picard--Fuchs operator
    i := idxs[1];
    if L[i]["PF"] eq M["PF"] then
	L[i]["names"] := SetToSequence(SequenceToSet(names) join SequenceToSet(L[i]["names"]));
    	printf "Merging: %o\n", L[i]["names"];
    else
	msg := Sprintf("Period sequences match but Picard--Fuchs operators do not: %o %o\n", names, L[i]["names"]);
	error msg;
    end if;
end procedure;

// Regularise an unregularised period sequence.
function regularise(ps)
    return [Integers() | Factorial(i - 1) * ps[i] : i in [1..#ps]];
end function;

// Unregularise a regularised period sequence.
function unregularise(ps)
    return [Rationals() | ps[i] / Factorial(i - 1) : i in [1..#ps]];
end function;

// Return the Fano manifold X that is a index-2 unsection of the Fano manifold M, with the given names.  The parameter c should be set to the degree-2 coefficient of the regularised quantum period of X.
function index_2_unsection(M, names, c : number_of_terms := 500)
    M_ps := PeriodSequenceForPicardFuchsOperator(M["PF"], Ceiling(number_of_terms/2), M["period"]);
    R<x> := PowerSeriesRing(Rationals() : Precision:=1+number_of_terms);
    unreg := R!unregularise(M_ps);
    // get the linear term right
    Ifun := Exp(c*x/2)*unreg;
    a := func<d|Coefficient(Ifun,d)>;
    reg_X := &+[Factorial(2*d)/Factorial(d)*a(d)*x^(2*d) : d in [0..Ceiling(number_of_terms/2)]];
    ps := [Integers()|Coefficient(reg_X,d) : d in [0..number_of_terms-1]];
    PF := PicardFuchsOperator(ps);
    result := AssociativeArray();														  
    result["period"] := ps[1..16];
    result["names"] := names;
    result["PF"] := PF;
    result["proven"] := false;
    result["proof"] := "\"\"";
    return result;
end function;

//////////////////////////////////////////////////////////////////////
// MW(11) was computed using the Abelian/non-Abelian correspondence.
// See Coates--Galkin--Kasprzyk--Strangeway section 6.2.11.
//////////////////////////////////////////////////////////////////////

function MW11()
    number_of_terms := 200;
    // cache the harmonic numbers
    H := [HarmonicNumber(n) : n in [1..Ceiling(number_of_terms/2)]];
    function harm(n)
	if n eq 0 then
	    return 0;
	else
	    return H[n];
	end if;
    end function;    
    // build the period sequence
    C := PositiveQuadrant(3);
    h := Dual(Ambient(C))![2,2,2];
    ps := [Rationals()|1];
    for d in [1..number_of_terms] do
	// the odd terms in the period sequence are zero
	if IsOdd(d) then
	    Append(~ps,0);
	    continue;
	end if;
	// compute the even terms
	coeff := 0;
	for p in Points(C,h,d) do
	    l, m, n := Explode(Eltseq(p));
	    if n ge l and n ge m then
		coeff +:= (-1)^(l+m)*Factorial(l+m)/(Factorial(l)^4*Factorial(m)^4*Factorial(n-l)*Factorial(n-m))*(1+(m-l)*(harm(n-m)-4*harm(m)));
	    end if;
	    if m gt l and n ge l and n lt m then
		coeff +:= (-1)^(l+n)*Factorial(l+m)/(Factorial(l)^4*Factorial(m)^4*Factorial(n-l))*Factorial(m-n-1)*(m-l);
	    end if;
	end for;
	Append(~ps, coeff);
    end for;
    // compute the Picard--Fuchs operator
    reg_ps := regularise(ps);
    PF := PicardFuchsOperator(reg_ps);
    // package up the data and return
    data := AssociativeArray();
    data["period"] := reg_ps[1..16];
    data["PF"] := PF;
    data["names"] := ["MW(11)"];
    data["proven"] := false;
    data["proof"] := "\"\"";
    return data;
end function;

//////////////////////////////////////////////////////////////////////
// The Strangeway 4-folds were computed using the Abelian/non-Abelian
// correspondence. See Coates--Galkin--Kasprzyk--Strangeway section 8.
// The Picard--Fuchs operators here were computed numerically using
// the output from hpcjobs/Strangeway_periods
//////////////////////////////////////////////////////////////////////
function Strangeway()
    printf "Computing quantum periods for the Strangeway 4-folds:\n";
    number_of_terms := 15;
    // cache the harmonic numbers 
    H := [HarmonicNumber(n) : n in [1..number_of_terms]];
    function harm(n)
	if n eq 0 then
	    return 0;
	else
	    return H[n];
	end if;
    end function;
    // cache the ingredients that we need
    c := ZeroMatrix(Rationals(),number_of_terms+1,number_of_terms+1);
    for l in [0..number_of_terms] do
	for m in [0..number_of_terms - l] do
	    coeff := 0;
	    for i in [0..l] do
		for j in [0..m] do
		    coeff +:= (-1)^(j+l) * Factorial(m+l-i-j)*Factorial(i+m-j)*Factorial(m+l-j)/(Factorial(l-i)^4*Factorial(i)^4*Factorial(m-j)^4*Factorial(j)*Factorial(m)*Factorial(l)^4)*(1+(2*i-l)*(harm(i+m-j)-4*harm(i)));
		end for;
	    end for;
	    c[l+1,m+1] := coeff;
	end for;
	printf "Caching data %o/%o\n", l, number_of_terms;
    end for;
    // compute the regularised period sequences
    R<x> := PowerSeriesRing(Rationals() : Precision:=number_of_terms);
    I1 := &+[Factorial(l)^5*c[l+1,m+1]*x^(l+2*m) : l in [0..number_of_terms], m in [0..number_of_terms] | l+2*m le number_of_terms];
    ps1 := regularise(Coefficients(Exp(-x)*I1));
    I2 := &+[Factorial(l)^4*Factorial(m)*c[l+1,m+1]*x^(2*l+m) : l in [0..number_of_terms], m in [0..number_of_terms] | 2*l+m le number_of_terms];
    ps2 := regularise(Coefficients(I2));
    I3 := &+[Factorial(l)^4*Factorial(l+m)*c[l+1,m+1]*x^(l+m) : l in [0..number_of_terms], m in [0..number_of_terms] | l+m le number_of_terms];
    ps3 := regularise(Coefficients(Exp(-x)*I3));
    // hard-coded Picard--Fuchs operators
    _<D,t> := WeylAlgebra();
    PF1 := 4659331054687500*t^20*D^6 + 11785032714843750*t^19*D^6 + 
	   97845952148437500*t^20*D^5 + 16157964257812500*t^18*D^6 + 
	   230848800292968750*t^19*D^5 + 815382934570312500*t^20*D^4 + 
	   14812265595703125*t^17*D^6 + 290845156054687500*t^18*D^5 + 
	   1812827424316406250*t^19*D^4 + 3424608325195312500*t^20*D^3 + 
	   9954108222656250*t^16*D^6 + 242384176611328125*t^17*D^5 + 
	   2128777022460937500*t^18*D^4 + 7247863674316406250*t^19*D^3 + 
	   7566753632812500000*t^20*D^2 + 5364224799218750*t^15*D^6 + 
	   146429782001953125*t^16*D^5 + 1636002187646484375*t^17*D^4 + 
	   8038122571289062500*t^18*D^3 + 15395593617187500000*t^19*D^2 + 
	   8219059980468750000*t^20*D + 2541711932890625*t^14*D^6 + 
	   73729159090234375*t^15*D^5 + 901037784316406250*t^16*D^4 + 
	   5773314697880859375*t^17*D^3 + 16321826669531250000*t^18*D^2 + 
	   16230290748046875000*t^19*D + 3354718359375000000*t^20 + 
	   1164531820890625*t^13*D^6 + 35752055702265625*t^14*D^5 + 
	   439066092339843750*t^15*D^4 + 2938326951451171875*t^16*D^3 + 
	   11095917446835937500*t^17*D^2 + 16632029257031250000*t^18*D + 
	   6488797148437500000*t^19 + 515681033618750*t^12*D^6 + 
	   17793192961234375*t^13*D^5 + 228053963911328125*t^14*D^4 + 
	   1431586199687890625*t^15*D^3 + 5287722295856250000*t^16*D^2 + 
	   10833845527617187500*t^17*D + 6494235328125000000*t^18 + 
	   202435244118750*t^11*D^6 + 8161622437031250*t^12*D^5 + 
	   121071511300859375*t^13*D^4 + 812060525558515625*t^14*D^3 + 
	   2635543519266250000*t^15*D^2 + 4898230568123437500*t^16*D + 
	   4102812502031250000*t^17 + 63946452356375*t^10*D^6 + 
	   3034193303984375*t^11*D^5 + 54519170744531250*t^12*D^4 + 
	   445616163717734375*t^13*D^3 + 1625161636802625000*t^14*D^2 + 
	   2524006929529687500*t^15*D + 1784273113181250000*t^16 + 
	   15059073958900*t^9*D^6 + 850367383415750*t^10*D^5 + 
	   18577817862018750*t^11*D^4 + 191791825529925000*t^12*D^3 + 
	   899565472257843750*t^13*D^2 + 1663964584369312500*t^14*D + 
	   949348451902500000*t^15 + 2435247087960*t^8*D^6 + 171985069232725*t^9*D^5 + 
	   4596200626031875*t^10*D^4 + 59755243305281875*t^11*D^3 + 
	   367925185337068750*t^12*D^2 + 917650568763750000*t^13*D + 
	   656022138057000000*t^14 + 235351282453*t^7*D^6 + 24262656000645*t^8*D^5 + 
	   810288373047750*t^9*D^4 + 13198255383632750*t^10*D^3 + 
	   105902757908369250*t^11*D^2 + 358819297934362500*t^12*D + 
	   359256221278875000*t^13 + 6961453783*t^6*D^6 + 2373645521135*t^7*D^5 + 
	   100051421231650*t^8*D^4 + 2069053849706825*t^9*D^3 + 
	   21223646212089150*t^10*D^2 + 96968008056166500*t^11*D + 
	   135806707320975000*t^12 - 1500446162*t^5*D^6 + 177553584577*t^6*D^5 + 
	   8890348918465*t^7*D^4 + 228956040788075*t^8*D^3 + 3014316559788480*t^9*D^2 +
	   17978854755245000*t^10*D + 35065251958401000*t^11 - 303607104*t^4*D^6 + 
	   15673340383*t^5*D^5 + 542797633601*t^6*D^4 + 18666085190075*t^7*D^3 + 
	   306082732421750*t^8*D^2 + 2362085992327960*t^9*D + 6138204510918600*t^10 - 
	   20986039*t^3*D^6 + 1451010576*t^4*D^5 + 10070744532*t^5*D^4 + 
	   1109720227893*t^6*D^3 + 23309475852112*t^7*D^2 + 224842815610700*t^8*D + 
	   761670398017080*t^9 - 1184719*t^2*D^6 + 84633413*t^3*D^5 - 
	   1517860010*t^4*D^4 + 37613988121*t^5*D^3 + 1260892461426*t^6*D^2 + 
	   16103821968520*t^7*D + 69262666755840*t^8 - 6488*t*D^6 + 4614316*t^2*D^5 - 
	   72060359*t^3*D^4 + 382688974*t^4*D^3 + 38236919410*t^5*D^2 + 
	   771777214080*t^6*D + 4705673567400*t^7 - 144*D^6 + 16872*t*D^5 - 
	   4360141*t^2*D^4 + 6733951*t^3*D^3 + 528702612*t^4*D^2 + 20744724356*t^5*D + 
	   198687396960*t^6 + 288*D^5 - 16224*t*D^4 + 27576*t^2*D^3 + 10229650*t^3*D^2 
	   + 357754272*t^4*D + 4798012320*t^5 - 144*D^4 + 80*t*D^3 + 18360*t^2*D^2 + 
	   6100544*t^3*D + 98068320*t^4 + 4608*t^2*D + 1399680*t^3;
    PF2 := 122216768941195448*t^22*D^6 + 497738488561514480*t^21*D^6 + 
	   2566552147765104408*t^22*D^5 + 1070335211705378416*t^20*D^6 + 
	   9336416424203562120*t^21*D^5 + 21387934564709203400*t^22*D^4 + 
	   1741857967418327568*t^19*D^6 + 18095442416474943848*t^20*D^5 + 
	   70362857964441404600*t^21*D^4 + 89829325171778654280*t^22*D^3 + 
	   2173300532653198346*t^18*D^6 + 26942942699851968392*t^19*D^5 + 
	   125525112991732355216*t^20*D^4 + 270969983067712576200*t^21*D^3 + 
	   198480032760501407552*t^22*D^2 + 2002753129337683482*t^17*D^6 + 
	   30547227081720546610*t^18*D^5 + 177969219745469529376*t^19*D^4 + 
	   453664696943334647640*t^20*D^3 + 557206642416545074520*t^21*D^2 + 
	   215590380412268770272*t^22*D + 1363486182556438844*t^16*D^6 + 
	   24132667040175771522*t^17*D^5 + 188713365148325377006*t^18*D^4 + 
	   630595761699387018360*t^19*D^3 + 890279275626125048544*t^20*D^2 + 
	   572201530871333245680*t^21*D + 87996073637660722560*t^22 + 
	   578923435532599478*t^15*D^6 + 12013296685082442688*t^16*D^5 + 
	   124977623085417741478*t^17*D^4 + 633183743776865858430*t^18*D^3 + 
	   1234909671353344433072*t^19*D^2 + 884534046577733521312*t^20*D + 
	   224440691493701390400*t^21 - 7266360064247600*t^14*D^6 + 
	   1443473737875444304*t^15*D^5 + 35597105362922070240*t^16*D^4 + 
	   340838672192979009142*t^17*D^3 + 1180113584060041313344*t^18*D^2 + 
	   1234602806598310727968*t^19*D + 339419462107980330624*t^20 - 
	   183386254459106997*t^13*D^6 - 3508160471039930070*t^14*D^5 - 
	   19354073109782886494*t^15*D^4 + 11796961018877102640*t^16*D^3 + 
	   499978214942700858912*t^17*D^2 + 1128708916990758137000*t^18*D + 
	   477520761931317424704*t^19 - 117295969676106705*t^12*D^6 - 
	   3160076409972432890*t^13*D^5 - 29361088368914300504*t^14*D^4 - 
	   122133014020310537224*t^15*D^3 - 134282450518712391972*t^16*D^2 + 
	   366799713365824344584*t^17*D + 421439638108324653344*t^18 - 
	   40050657086528172*t^11*D^6 - 1265163505899353810*t^12*D^5 - 
	   16478348780031106656*t^13*D^4 - 98536987797626816826*t^14*D^3 - 
	   283536856577556828480*t^15*D^2 - 238691783416014291032*t^16*D + 
	   104812461441522841376*t^17 - 7150313518682578*t^10*D^6 - 
	   257519878466944134*t^11*D^5 - 4721691993258795495*t^12*D^4 - 
	   39419242871611963244*t^13*D^3 - 163129725261866248448*t^14*D^2 - 
	   294996304754173473712*t^15*D - 117566802082306559392*t^16 - 
	   18370892298012*t^9*D^6 - 15012269164718274*t^10*D^5 - 
	   506335384768647306*t^11*D^4 - 7619038362882375358*t^12*D^3 - 
	   47068780535236488171*t^13*D^2 - 131907479262595383832*t^14*D - 
	   113393105145921793856*t^15 + 252347211029955*t^8*D^6 + 
	   7036562603484042*t^9*D^5 + 99412437666434149*t^10*D^4 + 
	   154334124413079634*t^11*D^3 - 4312509275724259400*t^12*D^2 - 
	   26000422917811889266*t^13*D - 41466094571209188416*t^14 + 
	   65961794687711*t^7*D^6 + 2090901540423490*t^8*D^5 + 
	   51060700756862466*t^9*D^4 + 520432927606767215*t^10*D^3 + 
	   1813917878295906866*t^11*D^2 + 1500067898846817616*t^12*D - 
	   4856903489060551192*t^13 + 6054514193853*t^6*D^6 + 382698052314196*t^7*D^5 +
	   10924933180665440*t^8*D^4 + 153084140199610018*t^9*D^3 + 
	   969542350598350586*t^10*D^2 + 2335694688757507384*t^11*D + 
	   1757500110859019008*t^12 + 722164484005*t^5*D^6 + 45673094147576*t^6*D^5 + 
	   2095930371063813*t^7*D^4 + 26329159521219787*t^8*D^3 + 
	   234015765967589746*t^9*D^2 + 855732882107795888*t^10*D + 
	   956632166852183192*t^11 - 35098717594*t^4*D^6 + 8361281512060*t^5*D^5 + 
	   255866389181406*t^6*D^4 + 4853339499444878*t^7*D^3 + 
	   39085384205100850*t^8*D^2 + 185627789937125164*t^9*D + 
	   293726901563947488*t^10 - 4497473407*t^3*D^6 + 498903076954*t^4*D^5 + 
	   25493769690324*t^5*D^4 + 576575903707545*t^6*D^3 + 7323074995558190*t^7*D^2 
	   + 32033386150643776*t^8*D + 59708886564725040*t^9 - 1361621185*t^2*D^6 + 
	   6017153460*t^3*D^5 - 164686066749*t^4*D^4 + 45510744669364*t^5*D^3 + 
	   771843030704618*t^6*D^2 + 5922316555792584*t^7*D + 10839924807119760*t^8 - 
	   12027256*t*D^6 + 5300431900*t^2*D^5 + 9311281787*t^3*D^4 + 
	   795714033629*t^4*D^3 + 45015759151085*t^5*D^2 + 540876117434536*t^6*D + 
	   1937898509470232*t^7 - 196320*D^6 + 33136968*t*D^5 - 5148624569*t^2*D^4 + 
	   15419272836*t^3*D^3 + 792459974766*t^4*D^2 + 24028702377102*t^5*D + 
	   154541369979184*t^6 + 392640*D^5 - 35320008*t*D^4 + 133496798*t^2*D^3 + 
	   18097155420*t^3*D^2 + 478866630544*t^4*D + 5457904551928*t^5 - 196320*D^4 + 
	   75256*t*D^3 + 104179680*t^2*D^2 + 10418525632*t^3*D + 128330170560*t^4 + 
	   40834560*t^2*D + 2455989376*t^3 + 6282240*t^2;
    PF3 := 63975127526912*t^18*D^6 + 495062235289088*t^17*D^6 + 1343477678065152*t^18*D^5 +
	   1696113858752640*t^16*D^6 + 9516391435503104*t^17*D^5 + 
	   11195647317209600*t^18*D^4 + 3381550722713024*t^15*D^6 + 
	   29551569995265664*t^16*D^5 + 73437158592074240*t^17*D^4 + 
	   47021718732280320*t^18*D^3 + 4334421889291520*t^14*D^6 + 
	   52744135292358592*t^15*D^5 + 209347393830552448*t^16*D^4 + 
	   289077924964221440*t^17*D^3 + 103895607103705088*t^18*D^2 + 
	   3720454754698624*t^13*D^6 + 59552433325137280*t^14*D^5 + 
	   339252350253820224*t^15*D^4 + 766261739176680320*t^16*D^3 + 
	   606000081356736512*t^17*D^2 + 112852124957472768*t^18*D + 
	   2155518870467304*t^12*D^6 + 44034810453419584*t^13*D^5 + 
	   342722241784883456*t^14*D^4 + 1142986860266371392*t^15*D^3 + 
	   1512996617385635840*t^16*D^2 + 632192934524389376*t^17*D + 
	   46062091819376640*t^18 + 826177465529108*t^11*D^6 + 
	   21262225959065224*t^12*D^5 + 221980542917087808*t^13*D^4 + 
	   1048231961503172736*t^14*D^3 + 2105737814028375808*t^15*D^2 + 
	   1506125088626612736*t^16*D + 250854948740014080*t^17 + 
	   198911818834702*t^10*D^6 + 6439723762698244*t^11*D^5 + 
	   90679390266342776*t^12*D^4 + 603469427670098240*t^13*D^3 + 
	   1778189765021550080*t^14*D^2 + 1983060175495635200*t^15*D + 
	   577898272723617792*t^16 + 28659384721180*t^9*D^6 + 1133678936636990*t^10*D^5
	   + 21766434866321628*t^11*D^4 + 210950622385892696*t^12*D^3 + 
	   922801077677541952*t^13*D^2 + 1564722337261803008*t^14*D + 
	   730419456049456128*t^15 + 4043229214865*t^8*D^6 + 141022981551136*t^9*D^5 + 
	   2755945602678722*t^10*D^4 + 39935123216254988*t^11*D^3 + 
	   278803838981648992*t^12*D^2 + 743019819973224576*t^13*D + 
	   547260303394387968*t^14 + 1414992465062*t^7*D^6 + 48233387759249*t^8*D^5 + 
	   459056101007376*t^9*D^4 + 3717990811949274*t^10*D^3 + 
	   41047471365508816*t^11*D^2 + 196207467399113824*t^12*D + 
	   242021982747414016*t^13 + 379502989367*t^6*D^6 + 18842500604715*t^7*D^5 + 
	   269476151271733*t^8*D^4 + 1339579523495104*t^9*D^3 + 
	   3176412703162144*t^10*D^2 + 21949773944880752*t^11*D + 
	   56781760975624576*t^12 + 46780131236*t^5*D^6 + 3606661938468*t^6*D^5 + 
	   86492847406800*t^7*D^4 + 808730725008103*t^8*D^3 + 2524707098606268*t^9*D^2 
	   + 1887316009334840*t^10*D + 4683631921113024*t^11 + 1991072195*t^4*D^6 + 
	   309376743270*t^5*D^5 + 12777411298785*t^6*D^4 + 206967511552469*t^7*D^3 + 
	   1296492546194994*t^8*D^2 + 2457251214474920*t^9*D + 604517678136160*t^10 - 
	   52652142*t^3*D^6 + 10108130673*t^4*D^5 + 857717183956*t^5*D^4 + 
	   24369099152908*t^6*D^3 + 272848179930890*t^7*D^2 + 1061871891272800*t^8*D + 
	   921569461106400*t^9 - 5294389*t^2*D^6 + 269623435*t^3*D^5 + 
	   21053043039*t^4*D^4 + 1269296384054*t^5*D^3 + 26611557002808*t^6*D^2 + 
	   190465459817712*t^7*D + 346443107066144*t^8 - 40236*t*D^6 + 20618412*t^2*D^5
	   + 104082672*t^3*D^4 + 20250541963*t^4*D^3 + 1111697806020*t^5*D^2 + 
	   15880922806832*t^6*D + 54794812098208*t^7 - 172*D^6 + 115376*t*D^5 - 
	   18397299*t^2*D^4 + 237624389*t^3*D^3 + 15365174634*t^4*D^2 + 
	   540452829080*t^5*D + 4014301365984*t^6 + 344*D^5 - 101616*t*D^4 + 
	   1324088*t^2*D^3 + 221194190*t^3*D^2 + 7480982248*t^4*D + 113899659968*t^5 - 
	   172*D^4 + 332*t*D^3 + 1029076*t^2*D^2 + 101129072*t^3*D + 1712092128*t^4 + 
	   388032*t^2*D + 18819008*t^3 + 55040*t^2;
    // wrap up the results and return
    results := [];
    for x in [[* ps1,["Str(1)"], PF1 *], [* ps2,["Str(2)"], PF2 *], [* ps3,["Str(3)"], PF3 *]] do
	ps := x[1];
	names := x[2];
	PF := x[3];
	// sanity check
	assert IsZero(ApplyPicardFuchsOperator(PF,ps));
	data := AssociativeArray();
	data["period"] := ps[1..16];
	data["PF"] := PF;
	data["names"] := names;
	data["proven"] := false;
	data["proof"] := "\"\"";
	Append(~results, data);
    end for;
    return results;
end function;

//////////////////////////////////////////////////////////////////////
// Building the key-value file
//////////////////////////////////////////////////////////////////////

// W is the Weyl algebra that contains the Picard--Fuchs operators
W<D,t> := WeylAlgebra();
    
// R is the ring that holds the Laurent polynomial mirrors
R<x,y,z,w> := RationalFunctionField(Rationals(),4);

// Read in and merge the output from step 1 
keys := {"period", "names", "PF", "proven", "proof", "f"};
conversion_funcs := AssociativeArray();
for k in keys do
    conversion_funcs[k] := func<xx|eval(xx)>;
end for;
input_file := Sprintf("%o/4d_Lairez_output.txt", get_db_dir());
results := [];
for M in KeyValueFileProcess(input_file, keys : skip_unknown:=false, require_all_keys:=true, conversion_funcs:=conversion_funcs) do
    merge_in(~results, M);													       
end for;

// Read in the Fano 3-folds
input_file := Sprintf("%o/Fano_3.txt", get_db_dir());
Fano3 := [ xx : xx in KeyValueFileProcess(input_file, keys : skip_unknown:=false, require_all_keys:=true, conversion_funcs:=conversion_funcs)];

// Read in and merge the toric complete intersections
input_file := Sprintf("%o/period_sequence_data.txt", get_ci_dir());
keys := {"Degree", "EulerNumber", "FanoIndex", "Hilb", "HilbDelta", "Laurent", "PFOperator", "Period", "PeriodID"};
conversion_funcs := AssociativeArray();
for k in keys do
    conversion_funcs[k] := func<xx|eval(xx)>;
end for;
for data in KeyValueFileProcess(input_file, keys : skip_unknown:=false, require_all_keys:=true, conversion_funcs:=conversion_funcs) do
    M := AssociativeArray();														  
    M["period"] := data["Period"][1..16];
    M["names"] := [Sprintf("CKP(%o)", data["PeriodID"])];
    M["PF"] := data["PFOperator"];
    M["proven"] := false;
    M["proof"] := "\"\"";
    M["f"] := data["Laurent"];
    merge_in(~results, M);
end for;

// Compute and merge in the unsections

// V(4,12) is an unsection of V(3,12).  The constant 10 below can be
// computed using Theorem F.1 in Coates--Corti--Galkin--Kasprzyk, much as
// in the computation for V(3,12) in ibid. section 13.
candidates := [M : M in Fano3 | "V(3,12)" in M["names"]];
assert #candidates eq 1;
M := candidates[1];
merge_in(~results, index_2_unsection(M, ["V(4,12)"], 10 : number_of_terms:=200));

// V(4,16) is an unsection of V(3,16).  The constant 8 below can be
// computed using Theorem F.1 in Coates--Corti--Galkin--Kasprzyk, much as
// in the computation for V(3,16) in ibid. section 15.
candidates := [M : M in Fano3 | "V(3,16)" in M["names"]];
assert #candidates eq 1;
M := candidates[1];
merge_in(~results, index_2_unsection(M, ["V(4,16)"], 8 : number_of_terms:=200));

// Abelian/non-Abelian correspondence calculation for the constant 8 for V(4,16):
/* 
R<p1,p2,p3> := PolynomialRing(Rationals(),3);
S := R/Ideal([p1^6,p2^6,p3^6]);

function g(p, l)
    return &*[S| p+m : m in [1..l]];
end function;

function summand(l1,l2,l3)
    return g(p1+p2+p3,l1+l2+l3)^2*g(p1+p2,l1+l2)*g(p1+p3,l1+l3)*g(p2+p3,l2+l3)*S!((p2-p1+l2-l1)*(p3-p1+l3-l1)*(p3-p2+l3-l2))/(g(p1,l1)^6*g(p2,l2)^6*g(p3,l3)^6);
end function;

function coeff(n)
    return &+[ summand(l1,l2,l3) : l1 in [0..n], l2 in [0..n], l3 in [0..n] | l1 + l2 + l3 eq n];
end function;

coeff(1)/coeff(0)
*/

// V(4,18) is an unsection of V(3,18).  The constant 6 below can be
// computed using Theorem F.1 in Coates--Corti--Galkin--Kasprzyk, much as
// in the computation for V(3,18) in ibid. section 16.
candidates := [M : M in Fano3 | "V(3,18)" in M["names"]];
assert #candidates eq 1;
M := candidates[1];
merge_in(~results, index_2_unsection(M, ["V(4,18)"], 6 : number_of_terms:=200));

// Abelian/non-Abelian correspondence calculation for the constant 6 for V(4,18):
/* 
R<p1,p2,p3,p4,p5> := PolynomialRing(Rationals(),5);
S := R/Ideal([p1^7,p2^7,p3^7,p4^7,p5^7]);

function g(p, l)
    return &*[S| p+m : m in [1..l]];
end function;

function summand(l1,l2,l3,l4,l5)
    p := [p1,p2,p3,p4,p5];
    l := [l1,l2,l3,l4,l5];
    num := g(&+p,&+l)*&*[g(&+p-p[i],&+l-l[i]) : i in [1..5]]*&*[S|p[j]-p[i]+l[j]-l[i] : j in [i+1..5], i in [1..4]];
    den := &*[g(p[i],l[i])^7 : i in [1..5]];
    return num/den;
end function;

function coeff(n)
    return &+[ summand(l1,l2,l3,l4,l5) : l1 in [0..n], l2 in [0..n], l3 in [0..n], l4 in [0..n], l5 in [0..n] | l1 + l2 + l3 + l4 + l5 eq n];
end function;

coeff(1)/coeff(0);
*/

// MW(11) was computed using the Abelian/non-Abelian correspondence.
// See Coates--Galkin--Kasprzyk--Strangeway section 6.2.11.
merge_in(~results, MW11());

// merge in the Strangeway 4-folds.  See Coates--Galkin--Kasprzyk--Strangeway section 8.
// The Picard--Fuchs operators here were computed numerically using the output from
// hpcjobs/Strangeway_periods
for data in Strangeway() do
    merge_in(~results, data);
end for;

// write out the key-value file
F := NewKeyValueFile(Sprintf("%o/Fano_4.txt", get_db_dir()));
for data in results do
    this_data := data;
    this_data["names"] := [ "\"" cat n cat "\"" : n in this_data["names"]];
    if this_data["proof"] eq "" then
	this_data["proof"] := "\"\"";
    end if;
    KeyValueWrite(F, this_data);
    KeyValueFlush(F);
end for;
