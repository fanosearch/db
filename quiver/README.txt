A database of all Fano quiver flag varieties of dimension up to 8 

This data was computed and assembled in 2018 by:

    Tom Coates <t.coates@imperial.ac.uk>
	Elana Kalashnikov <e.kalashnikov14@imperial.ac.uk>
	Alexander Kasprzyk <A.M.Kasprzyk@nottingham.ac.uk>

To the extent possible under law, Tom Coates, Elana Kalashnikov, and Alexander Kasprzyk, who associated CC0 with "A database of all Fano quiver
flag varieties of dimension up to 8" have waived all copyright and related or neighboring rights to "A database of all Fano quiver flag varieties
of dimension up to 8".

You should have received a copy of the CC0 Public Domain Dedication along with this work. If not, see
<http://creativecommons.org/publicdomain/zero/1.0/>.

If you make use of this data in an academic or commercial context, you should acknowledge this by including a reference or citation to the paper
"Four dimensional Fano quiver flag zero loci" by E. Kalashnikov, with an appendix by T. Coates, E. Kalashnikov, and A. Kasprzyk, 2018.

Each entry in the database has been assigned a unique ID in the range 1..223044.  Each entry is a quiver flag variety which is determined by a sequence of non-negative integers

[k, s, a_1, ..., a_k, b_11, ..., b_k1, b_12, ..., b_k2, ..., b_1k, ..., b_kk]

The first entry, k, in the sequence is the number of vertices in the quiver; the second entry, s, is a shift value; the next k values a_1, ..., a_k are such that the dimension vector of the quiver is

a_1 - s, ..., a_k - s

and the remaining k^2 values are such that the (i,j) entry of the adjacency matrix of the quiver is

b_ij - s.

To find the sequence corresponding to the quiver flag variety in the database with ID equal to N, write

N = 5000 q + r

with q and r non-negative integers and r in the range 0..4999.  We look in the block file with index q, at line r+1.  This contains an integer M.  The first line of the block file with index q contains an integer b, the base.  We express M in base b:

M = c_0 + c_1 b + c_2 b^2 + ...

and then the sequence [c_0, c_1, ... ], padded with zeroes as necessary, is the sequence 

[k, s, a_1, ..., a_k, b_11, ..., b_k1, b_12, ..., b_k2, ..., b_1k, ..., b_kk]

encoding the quiver flag variety in the manner described above.
