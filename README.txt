Common databases used by Magma and Sage.

ks4
---
If you have access to our version of the Kreuzer-Skarke database of reflexive
4-topes, "ks4", you should add a symlink in this directory.

   System                   Location
   ------                   --------
   Fano                     /data/fano/ks4
   Imperial HPC             /home/akasprzy/storage/ks4
   Imperial Maths Cluster   /home/calculus/home/fano/ks4
